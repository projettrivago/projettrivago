import Layout from "./components/Layout";
import { Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import Accueil from "./pages/Accueil";
import APropos from "./pages/APropos";
import NosVoyages from "./pages/NosVoyages";
import Contact from "./pages/Contact";

function App() {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact>
                    <Home />
                </Route>
                <Route path="/Accueil">
                    <Accueil />
                </Route>
                <Route path="/APropos">
                    <APropos />
                </Route>
                <Route path="/NosVoyages">
                    <NosVoyages />
                </Route>
                <Route path="/Contact">
                    <Contact />
                </Route>
            </Switch>
        </Layout>
    );
}

export default App;
